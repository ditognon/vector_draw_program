package vector_draw_program;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import vector_draw_program.helpClasses.Point;

public class DocumentModel {
    public final static double SELECTION_PROXIMITY = 10;

	// Kolekcija svih grafičkih objekata:
	private List<GraphicalObject> objects = new ArrayList<>();
	// Read-Only proxy oko kolekcije grafičkih objekata:
	private List<GraphicalObject> roObjects = Collections.unmodifiableList(objects);
	// Kolekcija prijavljenih promatrača:
	private List<DocumentModelListener> listeners = new ArrayList<>();
	// Kolekcija selektiranih objekata:
	private List<GraphicalObject> selectedObjects = new ArrayList<>();
	// Read-Only proxy oko kolekcije selektiranih objekata:
	private List<GraphicalObject> roSelectedObjects = Collections.unmodifiableList(selectedObjects);

	// Promatrač koji će biti registriran nad svim objektima crteža...
	private final GraphicalObjectListener goListener = new GraphicalObjectListener() {
        @Override
        public void graphicalObjectChanged(GraphicalObject go) {
            
            notifyListeners();
        }

        @Override
        public void graphicalObjectSelectionChanged(GraphicalObject go) {
            if(go.isSelected()) {
                selectedObjects.add(go);
            }else {
                selectedObjects.remove(go);
            }
            notifyListeners();
        }
    };
	
	// Konstruktor...
	public DocumentModel() {
        
    }

	// Brisanje svih objekata iz modela (pazite da se sve potrebno odregistrira)
	// i potom obavijeste svi promatrači modela
	public void clear() {
        ArrayList<GraphicalObject> forDelete = new ArrayList<>();
        forDelete.addAll(objects);

        for(GraphicalObject go : forDelete) {
            objects.remove(go);
            selectedObjects.remove(go);
            go.removeGraphicalObjectListener(goListener);
        }
        notifyListeners();
    }

	// Dodavanje objekta u dokument (pazite je li već selektiran; registrirajte model kao promatrača)
	public void addGraphicalObject(GraphicalObject obj) {
        if(obj.isSelected()) {
            selectedObjects.add(obj);
        }
        objects.add(obj);
        obj.addGraphicalObjectListener(goListener);
        
        notifyListeners();
    }
	
	// Uklanjanje objekta iz dokumenta (pazite je li već selektiran; odregistrirajte model kao promatrača)
	public void removeGraphicalObject(GraphicalObject obj) {
        obj.removeGraphicalObjectListener(goListener);

        if(obj.isSelected()) {
            selectedObjects.remove(obj);
        }

        objects.remove(obj);

        notifyListeners();
    }

	// Vrati nepromjenjivu listu postojećih objekata (izmjene smiju ići samo kroz metode modela)
	public List<GraphicalObject> list() {
        return Collections.unmodifiableList(objects);
    }

	// Prijava...
	public void addDocumentModelListener(DocumentModelListener l) {
        listeners.add(l);
    }
	
	// Odjava...
	public void removeDocumentModelListener(DocumentModelListener l) {
        listeners.remove(l);
    }

	// Obavještavanje...
	public void notifyListeners() {
        for(DocumentModelListener dml : listeners) {
            dml.documentChange();
        }
    }
	
	// Vrati nepromjenjivu listu selektiranih objekata
	public List<GraphicalObject> getSelectedObjects() {
        return Collections.unmodifiableList(selectedObjects);
    }

	// Pomakni predani objekt u listi objekata na jedno mjesto kasnije...
	// Time će se on iscrtati kasnije (pa će time možda veći dio biti vidljiv)
	public void increaseZ(GraphicalObject go) {
        if(objects.size() > 1) { // Provjera da nije jedini objekt u listi
            int indexOfGo = objects.indexOf(go);
            if(indexOfGo < objects.size() - 1) { // Provjera da nije zadnji element
                GraphicalObject tmp; // Sluzi za zamjenu
                tmp = go;
                objects.set(indexOfGo, objects.get(indexOfGo + 1));
                objects.set(indexOfGo + 1, tmp);
            }
            notifyListeners();
        }
    }
	
	// Pomakni predani objekt u listi objekata na jedno mjesto ranije...
	public void decreaseZ(GraphicalObject go) {
        if(objects.size() > 1) { // Provjera da nije jedini objekt u listi
            int indexOfGo = objects.indexOf(go);
            if(indexOfGo >= 1) { // Provjera da nije prvi element
                GraphicalObject tmp; // Sluzi za zamjenu
                tmp = go;
                objects.set(indexOfGo, objects.get(indexOfGo - 1));
                objects.set(indexOfGo - 1, tmp);
            }
            notifyListeners();
        }
    }
	
	// Pronađi postoji li u modelu neki objekt koji klik na točku koja je
	// predana kao argument selektira i vrati ga ili vrati null. Točka selektira
	// objekt kojemu je najbliža uz uvjet da ta udaljenost nije veća od
	// SELECTION_PROXIMITY. Status selektiranosti objekta ova metoda NE dira.
	public GraphicalObject findSelectedGraphicalObject(Point mousePoint) {
        GraphicalObject minObject = null;
        double minDistance = -1;

        double goDistance;;

        for(GraphicalObject go : objects) {
            goDistance = go.selectionDistance(mousePoint);
            if(goDistance < SELECTION_PROXIMITY) {
                if(minObject == null || goDistance < minDistance) {
                    minObject = go;
                    minDistance = goDistance;
                }
            }
        }

        return minObject;
    }

	// Pronađi da li u predanom objektu predana točka miša selektira neki hot-point.
	// Točka miša selektira onaj hot-point objekta kojemu je najbliža uz uvjet da ta
	// udaljenost nije veća od SELECTION_PROXIMITY. Vraća se indeks hot-pointa 
	// kojeg bi predana točka selektirala ili -1 ako takve nema. Status selekcije 
	// se pri tome NE dira.
	public int findSelectedHotPoint(GraphicalObject object, Point mousePoint) {
        int index = -1;
        double minDistance = -1;

        double hpDistance;

        for(int i = 0; i < object.getNumberOfHotPoints(); i++) {
            hpDistance = GeometryUtil.distanceFromPoint(object.getHotPoint(i), mousePoint);
            if(hpDistance < SELECTION_PROXIMITY) {
                if(index ==  -1 || hpDistance < minDistance) {
                    index = i;
                    minDistance = hpDistance;
                }
            }
        }

        return index;
    }

}
