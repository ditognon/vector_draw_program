package vector_draw_program;

public interface DocumentModelListener {
    void documentChange();
}
