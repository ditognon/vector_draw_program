# Java Vector Drawing Program

Simple vector drawing program build for lab exercise.

Warning: The program contains quite a lot of bugs!

# Running the program

To run the program just run Main.Java

The program automatically starts with the drawing screen.

# Drawing

From the toolbar choose which shape you want to draw then press on the drawing screen.

To resize the shape use the selection tool. Select the shape. And then by pulling on the small rectangles resize the shape!

To delete a shape use "brisalo". It works a bit different than other erase tools. You need to draw a line through the objects you want to delete, and it will delete all the objects that instersect with the line.

It is also possible to save and load files.
