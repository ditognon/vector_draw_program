package vector_draw_program;

import javax.swing.JComponent;
import javax.swing.event.MouseInputAdapter;

import vector_draw_program.renderers.G2DRendererImpl;
import vector_draw_program.renderers.Renderer;

import java.awt.*;
import java.awt.event.*;

public class Canvas extends JComponent implements DocumentModelListener{
    private DocumentModel model;
    private GUI gui;
    private boolean shift = false;
    private boolean ctrl = false;
    public Canvas(DocumentModel model, GUI gui) {
        model.addDocumentModelListener(this);
        this.model = model;
        this.gui = gui;
        this.setFocusable(true);
        
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
                    shift = true;
                }else if(e.getKeyCode() == KeyEvent.VK_CONTROL) {
                    ctrl = true;
                }else {
                    gui.getCurrentState().keyPressed(e.getKeyCode());
                }
            }

            public void keyReleased(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
                    shift = false;
                }else if(e.getKeyCode() == KeyEvent.VK_CONTROL) {
                    ctrl = false;
                }
            }
        });

        this.addMouseListener(new MouseInputAdapter(){
            @Override
            public void mousePressed(MouseEvent e) {
                gui.getCurrentState().mouseDown(new vector_draw_program.helpClasses.Point(e.getX(), e.getY()), shift, ctrl);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                gui.getCurrentState().mouseUp(new vector_draw_program.helpClasses.Point(e.getX(), e.getY()), shift, ctrl);
            }
        });

        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                gui.getCurrentState().mouseDragged(new vector_draw_program.helpClasses.Point(e.getX(), e.getY()));
            }
        });
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D graphics2d = (Graphics2D) g;

        Renderer r = new G2DRendererImpl(graphics2d);
        for(GraphicalObject go : model.list()) {
            go.render(r);
            gui.getCurrentState().afterDraw(r, go);
        }
        gui.getCurrentState().afterDraw(r);

    }

    @Override
    public void documentChange() {
        this.repaint();
    }
}
