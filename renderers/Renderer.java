package vector_draw_program.renderers;

import vector_draw_program.helpClasses.Point;

public interface Renderer {
	void drawLine(Point s, Point e);
	void fillPolygon(Point[] points);
}
