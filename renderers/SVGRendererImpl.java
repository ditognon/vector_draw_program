package vector_draw_program.renderers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import vector_draw_program.helpClasses.Point;

public class SVGRendererImpl implements Renderer{
    private ArrayList<String> lines;

    public SVGRendererImpl(String filename) {
        lines = new ArrayList<>();
        lines.add("<svg>");
    }

    public void close() throws IOException {
        lines.add("</svg>");
        
        try {
            PrintWriter writer = new PrintWriter("file.svg", "UTF-8");
            
            for(String line : lines) {
                writer.write(line);
                writer.println();
            }

            writer.close();
            System.out.println("Successfully written SVG");
          } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }


	}

    @Override
    public void drawLine(Point s, Point e) {
        lines.add(String.format("<line x1=\"%d\"  y1=\"%d\" x2=\"%d\"   y2=\"%d\" style=\"stroke:#260FF7;\"/>",
         s.getX(), s.getY(), e.getX(), e.getY()));
    }

    @Override
    public void fillPolygon(Point[] points) {
        String pointsString = "";
        for(Point p : points) {
            pointsString += " " + p.getX() + "," + p.getY();
        }

        lines.add(String.format("<polygon points=%s style=\"stroke:#260FF7; fill:#F70F0F;\"/>", pointsString));
    }
}
