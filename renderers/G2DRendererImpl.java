package vector_draw_program.renderers;

import java.awt.*;
import vector_draw_program.helpClasses.Point;

public class G2DRendererImpl implements Renderer{
    private Graphics2D g2d;
    public G2DRendererImpl(Graphics2D g2d) {
        this.g2d = g2d;
    }

    @Override
    public void drawLine(Point s, Point e) {
        g2d.setColor(Color.BLUE);
        g2d.drawLine(s.getX(), s.getY(), e.getX(), e.getY());
    }

    @Override
    public void fillPolygon(Point[] points) {   
        int n = points.length;
        int x[] = new int[points.length];
        int y[] = new int[points.length];

        for(int i = 0; i < points.length; i++) {
            x[i] = points[i].getX();
            y[i] = points[i].getY();
        }

        g2d.setColor(Color.BLUE);
        g2d.fillPolygon(x, y, n);

        g2d.setColor(Color.RED);
        g2d.drawPolygon(x, y, n);
    }

}
