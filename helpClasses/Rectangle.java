package vector_draw_program.helpClasses;

public class Rectangle {
	private int x;
	private int y;
	private int width;
	private int height;
	private Point upperLeft;
	private Point upperRight;
	private Point lowerLeft;
	private Point lowerRight;
	
	public Rectangle(int x, int y, int width, int height) {
		this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

		upperLeft = new Point(x, y);
        upperRight = new Point(x + width, y);
        lowerRight = new Point(x + width, y + height);
        lowerLeft = new Point(x, y + height);
	};
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}

	public Point getUpperLeft() {
		return upperLeft;
	}

	public Point getUpperRight() {
		return upperRight;
	}

	public Point getLowerLeft() {
		return lowerLeft;
	}

	public Point getLowerRight() {
		return lowerRight;
	}

	// Rectangle rectangle = new Rectangle(100, 100, 100, 100);
	// // GORNJA CRTA
	// graphics2d.setColor(Color.RED);
	// graphics2d.drawLine(rectangle.getUpperLeft().getX(), rectangle.getUpperLeft().getY(), 
	// rectangle.getUpperRight().getX(), rectangle.getUpperRight().getY());

	// // DONJA CRTA
	// graphics2d.setColor(Color.BLUE);
	// graphics2d.drawLine(rectangle.getLowerLeft().getX(), rectangle.getLowerLeft().getY(), 
	// rectangle.getLowerRight().getX(), rectangle.getLowerRight().getY());

	// // LIJEVA CRTA
	// graphics2d.setColor(Color.GREEN);
	// graphics2d.drawLine(rectangle.getUpperLeft().getX(), rectangle.getUpperLeft().getY(), 
	// rectangle.getLowerLeft().getX(), rectangle.getLowerLeft().getY());

	// // DESNA CRTA
	// graphics2d.setColor(Color.YELLOW);
	// graphics2d.drawLine(rectangle.getUpperRight().getX(), rectangle.getUpperRight().getY(), 
	// rectangle.getLowerRight().getX(), rectangle.getLowerRight().getY());
}
