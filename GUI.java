package vector_draw_program;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

import javax.swing.*;

import vector_draw_program.states.AddShapeState;
import vector_draw_program.states.EraseState;
import vector_draw_program.states.IdleState;
import vector_draw_program.states.SelectShapeState;
import vector_draw_program.states.State;
import vector_draw_program.graphicalObjects.*;
import vector_draw_program.renderers.SVGRendererImpl;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;


public class GUI extends JFrame{
    private DocumentModel model;
    private Canvas canvas;
    private State currentState;

    public GUI(List<GraphicalObject> objects) {
        model = new DocumentModel();
        currentState = IdleState.getInstance();
        canvas = new Canvas(model, this);

        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(540, 400);

        JToolBar toolbar = new JToolBar();

        JButton selectButton = new JButton("Selektiraj");
        selectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentState = new SelectShapeState(model);
            }
        });
        toolbar.add(selectButton);

        JButton eraseButton = new JButton("Brisalo");
        eraseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentState = new EraseState(model);
            }
        });
        toolbar.add(eraseButton);

        GUI gui = this;
        JFileChooser chooserSave = new JFileChooser();

        JButton saveButton = new JButton("Pohrani");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = chooserSave.showSaveDialog(gui);
                if(result ==  JFileChooser.APPROVE_OPTION) {
                    File file = chooserSave.getSelectedFile();

                    ArrayList<String> lines = new ArrayList<>();
                    for(GraphicalObject go : model.list()) {
                        go.save(lines);
                    }

                    try {
                        PrintWriter writer = new PrintWriter("save.txt", "UTF-8");
                        
                        for(String line : lines) {
                            writer.write(line);
                            writer.println();
                        }
            
                        writer.close();
                        System.out.println("Successfully saved");
                      } catch (IOException ex) {
                        System.out.println("An error occurred.");
                        ex.printStackTrace();
                      }
                }
            }
        });
        toolbar.add(saveButton);

        JFileChooser chooserLoad = new JFileChooser();

        JButton loadButton = new JButton("Ucitaj");
        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Stack<GraphicalObject> objectsStack = new Stack<>();
                ArrayList<String> lines = new ArrayList<>();
                HashMap<String, GraphicalObject> mapa = new HashMap<String, GraphicalObject>();
                mapa.put("@OVAL", new Oval());
                mapa.put("@LINE", new LineSegment());
                mapa.put("@COMP", new CompositeShape());

                int res = chooserLoad.showOpenDialog(gui);
                if(res == JFileChooser.APPROVE_OPTION) {
                    File file = chooserLoad.getSelectedFile();

                    try {
                        Scanner myReader = new Scanner(file);
                        while (myReader.hasNextLine()) {
                            lines.add(myReader.nextLine());
                        }
                        myReader.close();

                        for(String line : lines) {
                            String header = line.substring(0, 5);
                            String rest = line.substring(6);

                            mapa.get(header).load(objectsStack, rest);
                        }

                        model.clear();

                        while(!objectsStack.empty()) {
                            GraphicalObject go = objectsStack.pop();
                            model.addGraphicalObject(go);
                        }

                      } catch (FileNotFoundException er) {
                        er.printStackTrace();
                      }

                }
                
            }
        });
        toolbar.add(loadButton);

        JFileChooser chooserSvg = new JFileChooser();

        JButton svgButton = new JButton("SVG export");
        svgButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = chooserSvg.showSaveDialog(gui);
                if(result ==  JFileChooser.APPROVE_OPTION) {
                    File file = chooserSvg.getSelectedFile();
                    SVGRendererImpl r = new SVGRendererImpl(chooserSvg.getName());
                    for(GraphicalObject go : model.list()) {
                        go.render(r);
                    }
                    try {
                        r.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        toolbar.add(svgButton);


        for(GraphicalObject go : objects) {
            JButton tmpButton = new JButton(go.getShapeName());
            tmpButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    currentState = new AddShapeState(go, model);
                }
            });
            toolbar.add(tmpButton);
        }


        this.add(toolbar, BorderLayout.NORTH);
        this.add(canvas, BorderLayout.CENTER);

        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    currentState = IdleState.getInstance();
                }
            }
        });
    }

    public State getCurrentState() {
        return currentState;
    }
}
