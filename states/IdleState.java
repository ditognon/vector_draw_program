package vector_draw_program.states;

import vector_draw_program.renderers.Renderer;

import vector_draw_program.GraphicalObject;
import vector_draw_program.helpClasses.Point;

// Prazni Adapter za State kako ne bi u svakom state-u morali sve implementirati
public class IdleState implements State{
    private static State instance;

    private IdleState() {};

    @Override
    public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
        // Not implemented
        
    }

    @Override
    public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
        // Not implemented
        
    }

    @Override
    public void mouseDragged(Point mousePoint) {
        // Not implemented
        
    }

    @Override
    public void keyPressed(int keyCode) {
        // Not implemented
        
    }

    @Override
    public void afterDraw(Renderer r, GraphicalObject go) {
        // Not implemented
        
    }

    @Override
    public void afterDraw(Renderer r) {
        // Not implemented
        
    }

    @Override
    public void onLeaving() {
        // Not implemented
        
    }

    public static State getInstance() {
        if(instance == null) {
            instance = new IdleState();
        }
        return instance;
    }
    
}
