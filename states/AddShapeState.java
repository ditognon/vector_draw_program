package vector_draw_program.states;

import vector_draw_program.DocumentModel;
import vector_draw_program.GraphicalObject;
import vector_draw_program.helpClasses.Point;

public class AddShapeState extends StateAdapter {
    private GraphicalObject prototype;
    private DocumentModel model;

    public AddShapeState(GraphicalObject prototype, DocumentModel model) {
        this.prototype = prototype;
        this.model = model;
    }

    @Override
    public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
        GraphicalObject tmp = prototype.duplicate();
        tmp.translate(mousePoint);
        model.addGraphicalObject(tmp);
    }
    
}
