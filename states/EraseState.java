package vector_draw_program.states;

import java.util.ArrayList;

import vector_draw_program.DocumentModel;
import vector_draw_program.GraphicalObject;
import vector_draw_program.graphicalObjects.LineSegment;
import vector_draw_program.helpClasses.Point;
import vector_draw_program.renderers.Renderer;

public class EraseState extends StateAdapter{
    private DocumentModel model;
    private ArrayList<GraphicalObject> forErase;
    private ArrayList<GraphicalObject> visual;

    public EraseState(DocumentModel model) {
        this.model =model;
        forErase = new ArrayList<GraphicalObject>();
        visual = new ArrayList<GraphicalObject>();
    }


    @Override
    public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
        for(GraphicalObject go : forErase) {
            model.removeGraphicalObject(go);
        }

        forErase.clear();

        visual.clear();
    }

    @Override
    public void mouseDragged(Point mousePoint) {
        GraphicalObject go = model.findSelectedGraphicalObject(mousePoint);
        if(go != null) {
            forErase.add(go);
        }

        if(visual.size() >= 1) {
            GraphicalObject last = visual.get(visual.size() - 1);
            visual.add(new LineSegment(new Point(last.getHotPoint(1)), mousePoint));
        }else {
            visual.add(new LineSegment(mousePoint, mousePoint));
        }

        model.notifyListeners();

    }

    @Override
    public void afterDraw(Renderer r) {
        for(GraphicalObject v : visual) {
            r.drawLine(v.getHotPoint(0), v.getHotPoint(1));
        }
        
    }
    
}
