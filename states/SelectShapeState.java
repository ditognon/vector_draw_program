package vector_draw_program.states;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import vector_draw_program.DocumentModel;
import vector_draw_program.GraphicalObject;
import vector_draw_program.graphicalObjects.CompositeShape;
import vector_draw_program.helpClasses.Point;
import vector_draw_program.helpClasses.Rectangle;
import vector_draw_program.renderers.Renderer;

public class SelectShapeState extends StateAdapter{
    private DocumentModel model;

    public SelectShapeState(DocumentModel model) {
        this.model = model;
    }

    @Override
    public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
        if(ctrlDown) {
            System.out.println("USLI U CTRL");
            GraphicalObject go = model.findSelectedGraphicalObject(mousePoint);
            if(go != null) {
                if(go.isSelected()) {
                    go.setSelected(false);
                }else {
                    go.setSelected(true);
                }
            }
            
        }else {
            GraphicalObject go = model.findSelectedGraphicalObject(mousePoint);
            ArrayList<GraphicalObject> tmpList = new ArrayList<>();
            tmpList.addAll(model.getSelectedObjects());
            for(GraphicalObject tmp : tmpList) {
                tmp.setSelected(false);
            }
            if(go != null) go.setSelected(true);
        }
    }

    @Override
    public void mouseDragged(Point mousePoint) {
        GraphicalObject go = model.findSelectedGraphicalObject(mousePoint);
        if(go != null) {
            int index = model.findSelectedHotPoint(go, mousePoint);
            if(index != -1) {
                Point diff = mousePoint.difference(go.getHotPoint(index));
                go.setHotPoint(index, go.getHotPoint(index).translate(diff));
            }
        }
        
    }

    private void moveObjects(Point translate) {
        for(GraphicalObject go : model.getSelectedObjects()) {
            for(int i = 0; i < go.getNumberOfHotPoints(); i++) {
                go.setHotPoint(i, go.getHotPoint(i).translate(translate));
            }
        }
    }

    private void changeZ(boolean plus) {
        System.out.println("ChangeZ: " + plus);
        for(GraphicalObject go : model.getSelectedObjects()) {
            if(plus) model.increaseZ(go);
            else model.decreaseZ(go);
        }
    }

    private void group() {
        ArrayList<GraphicalObject> selectedObjects = new ArrayList<>();
        CompositeShape kompozit = new CompositeShape();
        selectedObjects.addAll(model.getSelectedObjects());
        for(GraphicalObject go : selectedObjects) {
            model.removeGraphicalObject(go);
            kompozit.addGraphicalObject(go);
        }
        model.addGraphicalObject(kompozit);
    }

    private void ungroup() {
        ArrayList<GraphicalObject> selectedObjects = new ArrayList<>();
        selectedObjects.addAll(model.getSelectedObjects());
        if(selectedObjects.size() == 1) {
            GraphicalObject kompozit = selectedObjects.get(0);
            for(GraphicalObject go : kompozit.getSubObjects()) {
                model.addGraphicalObject(go); // Mozda postavit selekciju
            }

            model.removeGraphicalObject(kompozit);
        }
    }

    @Override
    public void keyPressed(int keyCode) {
        System.out.println(keyCode);
        switch(keyCode) {
            case KeyEvent.VK_UP:
                moveObjects(new Point(0, -1));
                break;
            case KeyEvent.VK_DOWN:
                moveObjects(new Point(0, 1));
                break;
            case KeyEvent.VK_LEFT:
                moveObjects(new Point(-1, 0));
                break;
            case KeyEvent.VK_RIGHT:
                moveObjects(new Point(1, 0));
                break;
            case 61:
                changeZ(true);
                break;
            case KeyEvent.VK_MINUS:
                changeZ(false);
                break;
            case KeyEvent.VK_G:
                group();
                break;
            case KeyEvent.VK_U:
                ungroup();
                break;
        }
        
    }

    @Override
    public void afterDraw(Renderer r, GraphicalObject go) {
        if(go.isSelected()) {
            Rectangle bb = go.getBoundingBox();
            r.drawLine(bb.getUpperLeft(), bb.getUpperRight());
            r.drawLine(bb.getUpperRight(), bb.getLowerRight());
            r.drawLine(bb.getLowerLeft(), bb.getLowerRight());
            r.drawLine(bb.getUpperLeft(), bb.getLowerLeft());

            // Treba i hotpointove oznacit
            if(model.getSelectedObjects().size() == 1) {
                for(int i = 0; i < go.getNumberOfHotPoints(); i++) {
                    Point hotPoint = go.getHotPoint(i);
                    int ss = (int)DocumentModel.SELECTION_PROXIMITY;

                    Rectangle hpbb = new Rectangle(hotPoint.getX() - (ss / 2), hotPoint.getY() - (ss / 2),
                     ss, ss);

                    r.drawLine(hpbb.getUpperLeft(), hpbb.getUpperRight());
                    r.drawLine(hpbb.getUpperRight(), hpbb.getLowerRight());
                    r.drawLine(hpbb.getLowerLeft(), hpbb.getLowerRight());
                    r.drawLine(hpbb.getUpperLeft(), hpbb.getLowerLeft());
                }
            }
        }
    }
    
}
