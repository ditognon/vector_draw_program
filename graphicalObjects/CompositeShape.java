package vector_draw_program.graphicalObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import vector_draw_program.GraphicalObject;
import vector_draw_program.GraphicalObjectListener;
import vector_draw_program.helpClasses.Point;
import vector_draw_program.helpClasses.Rectangle;
import vector_draw_program.renderers.Renderer;

public class CompositeShape implements GraphicalObject{
    private List<GraphicalObject> elements;
    private boolean selected;
    protected List<GraphicalObjectListener> listeners = new ArrayList<>();

    public CompositeShape() {
        super();
        elements = new ArrayList<>();
    }

    public CompositeShape(List<GraphicalObject> elements) {
        this.elements = elements;
    }

    public void addGraphicalObject(GraphicalObject go) {
        elements.add(go);
    }

    public void removeGraphicalObject(GraphicalObject go) {
        elements.remove(go);
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
        notifyGraphicalSelectionChangedListeners();
        
    }

    @Override
    public int getNumberOfHotPoints() {
        // int n = 0;
        // for(GraphicalObject go : elements) {
        //     n += go.getNumberOfHotPoints();
        // }

        return 0;
    }

    @Override
    public Point getHotPoint(int index) {
        for(GraphicalObject go : elements) {
            if(index > go.getNumberOfHotPoints()) {
                index -= go.getNumberOfHotPoints();
            }else {
                return go.getHotPoint(index);
            }
        }
        return null;
    }

    @Override
    public void setHotPoint(int index, Point point) {
        for(GraphicalObject go : elements) {
            if(index > go.getNumberOfHotPoints()) {
                index -= go.getNumberOfHotPoints();
            }else {
                go.setHotPoint(index, point);
            }
        }
        notifyGraphicalObjectChangedListeners();
    }

    @Override
    public boolean isHotPointSelected(int index) {
        return false;
    }

    @Override
    public void setHotPointSelected(int index, boolean selected) {
    }

    @Override
    public double getHotPointDistance(int index, Point mousePoint) {
        return 0;
    }

    @Override
    public void translate(Point delta) {
        for(GraphicalObject go : elements) {
            go.translate(delta);
        }
        notifyGraphicalObjectChangedListeners();
    }

    @Override
    public Rectangle getBoundingBox() {
        java.awt.Rectangle union = new java.awt.Rectangle();
        for(GraphicalObject go : elements) {
            Rectangle tmp = go.getBoundingBox();
            java.awt.Rectangle awtRect = new java.awt.Rectangle(tmp.getX(), tmp.getY(), tmp.getWidth(), tmp.getHeight());
            union = union.union(awtRect);
        }
        return new Rectangle((int)union.getX(), (int)union.getY(), (int)union.getWidth(), (int)union.getHeight());
    }

    @Override
    public double selectionDistance(Point mousePoint) {
        double minDistance = -1;
        double tmpDistance = -1;
        for(GraphicalObject go : elements) {
            tmpDistance = go.selectionDistance(mousePoint);
            if(tmpDistance < minDistance || minDistance == -1) {
                minDistance = tmpDistance; 
            }
        }

        return minDistance;
    }

    @Override
    public void render(Renderer r) {
        for(GraphicalObject go : elements) {
            go.render(r);
        }
        
    }

    @Override
    public void addGraphicalObjectListener(GraphicalObjectListener l) {
        listeners.add(l);
        
    }

    @Override
    public void removeGraphicalObjectListener(GraphicalObjectListener l) {
        listeners.remove(l);
        
    }

    @Override
    public String getShapeName() {
        return "Kompozit";
    }

    @Override
    public GraphicalObject duplicate() {
        System.out.println("POPRAVITI DUPLICATE COMPOSITE SHAPE");
        return new CompositeShape();
    }

    @Override
    public String getShapeID() {
        return "@COMP";
    }

    @Override
    public void load(Stack<GraphicalObject> stack, String data) {
        int nOfElements = Integer.parseInt(data);

        ArrayList<GraphicalObject> list = new ArrayList<>();
        for(int i = 0; i < nOfElements; i++) {
            list.add(stack.pop());
        }

        stack.push(new CompositeShape(list));
    }

    @Override
    public void save(List<String> rows) {
        for(GraphicalObject go : elements) {
            go.save(rows);
        }
        rows.add(getShapeID() + " " + elements.size());
    }

    @Override
    public List<GraphicalObject> getSubObjects() {
        return elements;
    }

    protected void notifyGraphicalObjectChangedListeners() {
        for(GraphicalObjectListener l : listeners) {
            l.graphicalObjectChanged(this);
        }
    }
    
    protected void notifyGraphicalSelectionChangedListeners() {
        for(GraphicalObjectListener l : listeners) {
            l.graphicalObjectSelectionChanged(this);
        }
    }
    
}
