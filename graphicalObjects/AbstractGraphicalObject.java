package vector_draw_program.graphicalObjects;

import java.util.ArrayList;
import java.util.List;

import vector_draw_program.GeometryUtil;
import vector_draw_program.GraphicalObject;
import vector_draw_program.GraphicalObjectListener;
import vector_draw_program.helpClasses.*;

public abstract class AbstractGraphicalObject implements GraphicalObject{
    protected Point[] hotPoints;
    protected boolean[] hotPointSelected;
    protected boolean selected;
    protected List<GraphicalObjectListener> listeners = new ArrayList<>();

    public AbstractGraphicalObject(Point[] points) {
        this.hotPoints = points;
    }

    @Override
    public boolean isSelected() {
        return this.selected;
    }
    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
        notifyGraphicalSelectionChangedListeners();
        
    }
    @Override
    public int getNumberOfHotPoints() {
        return this.hotPoints.length;
    }
    @Override
    public Point getHotPoint(int index) {
        if(index < hotPoints.length) return this.hotPoints[index];
        return null;
    }
    @Override
    public void setHotPoint(int index, Point point) {
        if(index < hotPoints.length) hotPoints[index] = point;
        notifyGraphicalObjectChangedListeners();
    }
    @Override
    public boolean isHotPointSelected(int index) {
        if(index < hotPointSelected.length) return hotPointSelected[index];
        return false;
    }
    @Override
    public void setHotPointSelected(int index, boolean selected) {
        if(index < hotPointSelected.length) hotPointSelected[index] = selected;
        
    }
    @Override
    public double getHotPointDistance(int index, Point mousePoint) {
        if(index < hotPoints.length) return GeometryUtil.distanceFromPoint(hotPoints[index], mousePoint);
        return -1;
    }
    @Override
    public void translate(Point delta) {
        for(int i = 0; i < hotPoints.length; i++) {
            hotPoints[i] = hotPoints[i].translate(delta);
        }
        notifyGraphicalObjectChangedListeners();
    }

    @Override
    public void addGraphicalObjectListener(GraphicalObjectListener l) {
        listeners.add(l);
        
    }
    @Override
    public void removeGraphicalObjectListener(GraphicalObjectListener l) {
        listeners.remove(l);
        
    }

    protected void notifyGraphicalObjectChangedListeners() {
        for(GraphicalObjectListener l : listeners) {
            l.graphicalObjectChanged(this);
        }
    }
    
    protected void notifyGraphicalSelectionChangedListeners() {
        for(GraphicalObjectListener l : listeners) {
            l.graphicalObjectSelectionChanged(this);
        }
    }

    public List<GraphicalObject> getSubObjects() {
        return null;
    }


}
