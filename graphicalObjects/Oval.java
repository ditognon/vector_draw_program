package vector_draw_program.graphicalObjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import vector_draw_program.*;
import vector_draw_program.helpClasses.*;
import vector_draw_program.renderers.Renderer;

public class Oval extends AbstractGraphicalObject {
    public Oval() {
        super(new Point[]{new Point(30, 0), new Point(0, 30)});
    }

    public Oval(Point right, Point down) {
        super(new Point[]{right, down});
    }

    @Override
    public double selectionDistance(Point mousePoint) {
        Rectangle bb = this.getBoundingBox();

        if(mousePoint.getX() > bb.getUpperLeft().getX() && mousePoint.getX() < bb.getUpperRight().getX()) {
            if(mousePoint.getY() < bb.getLowerLeft().getY() && mousePoint.getY() > bb.getUpperLeft().getY()) {
                return 0;
            }

        }
        System.out.println();
        System.out.println();
        double distUpper = GeometryUtil.distanceFromLineSegment(bb.getUpperLeft(), bb.getUpperRight(), mousePoint);
        double distLower = GeometryUtil.distanceFromLineSegment(bb.getLowerLeft(), bb.getLowerRight(), mousePoint);
        double distRight = GeometryUtil.distanceFromLineSegment(bb.getLowerRight(), bb.getUpperRight(), mousePoint);
        double distLeft = GeometryUtil.distanceFromLineSegment(bb.getLowerLeft(), bb.getUpperLeft(), mousePoint);

        return Math.min(distUpper, Math.min(distLower, Math.min(distRight, distLeft)));
    }

    @Override
    public Rectangle getBoundingBox() {
        Point difference = hotPoints[1].difference(hotPoints[0]);
        int rectX = hotPoints[1].getX() - Math.abs(difference.getX()); 
        int rectY = hotPoints[0].getY() - Math.abs(difference.getY());
        int rectWidth = 2 * Math.abs(difference.getX());
        int rectHeight = 2 * Math.abs(difference.getY());

        return new Rectangle(rectX, rectY, rectWidth, rectHeight);
    }

    @Override
    public GraphicalObject duplicate() {
        return new Oval(this.hotPoints[0], this.hotPoints[1]);
    }

    @Override
    public String getShapeName() {
        return "Oval";
    }
    
    @Override
    public String getShapeID() {
        return "@OVAL";
    }

    @Override
    public void render(Renderer r) {
        Point difference = hotPoints[1].difference(hotPoints[0]);
        int a = Math.abs(difference.getX());
        int b = Math.abs(difference.getY());
        int h = hotPoints[1].getX();
        int k = hotPoints[0].getY();

        int xMin = h - Math.abs(difference.getX());
        int xMax = h + Math.abs(difference.getX());
        ArrayList<Point> points = new ArrayList<>();
        ArrayList<Point> pointsUpper = new ArrayList<>();
        
        double calcY1, calcY2;
        for(int i = xMin; i <= xMax; i++) {

            double A = 1;
            double B = -2 * k;
            double C = Math.pow(k, 2) - Math.pow(b, 2) + (Math.pow(b, 2) / Math.pow(a, 2)) * Math.pow(i - h, 2);

            calcY1 = (-B + Math.sqrt(Math.pow(B, 2) - 4 * A * C)) / (2 * A);
            calcY2 = (-B - Math.sqrt(Math.pow(B, 2) - 4 * A * C)) / (2 * A);

            points.add(new Point(i, (int)calcY1));
            pointsUpper.add(new Point(i, (int)calcY2));
        }

        Collections.reverse(pointsUpper);
        points.addAll(pointsUpper);

        Point[] pointsArray = new Point[points.size()];
        for(int i = 0; i < points.size(); i++) {
            pointsArray[i] = points.get(i);
        }

        r.fillPolygon(pointsArray);
    }

    @Override
    public void load(Stack<GraphicalObject> stack, String data) {
        String[] params = data.split(" ");

        int x1 = Integer.parseInt(params[0]);
        int y1 = Integer.parseInt(params[1]);
        int x2 = Integer.parseInt(params[2]);
        int y2 = Integer.parseInt(params[3]);

        stack.push(new Oval(new Point(x1, y1), new Point(x2, y2)));
    }

    @Override
    public void save(List<String> rows) {
        rows.add(getShapeID() + " " + hotPoints[0].getX() + " " +
        hotPoints[0].getY() + " " + hotPoints[1].getX() + " " + 
        hotPoints[1].getY());
    }
    
}
