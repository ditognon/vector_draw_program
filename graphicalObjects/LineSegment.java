package vector_draw_program.graphicalObjects;

import java.util.List;
import java.util.Stack;

import vector_draw_program.GeometryUtil;
import vector_draw_program.GraphicalObject;
import vector_draw_program.helpClasses.Point;
import vector_draw_program.helpClasses.Rectangle;
import vector_draw_program.renderers.Renderer;

public class LineSegment extends AbstractGraphicalObject{

    public LineSegment() {
        super(new Point[]{new Point(0, 0), new Point(30, 0)});
    }

    public LineSegment(Point start, Point end) {
        super(new Point[]{start, end});
    }

    @Override
    public double selectionDistance(Point mousePoint) {
        return GeometryUtil.distanceFromLineSegment(hotPoints[0], hotPoints[1], mousePoint);
    }

    @Override
    public Rectangle getBoundingBox() {
        Point diff = hotPoints[0].difference(hotPoints[1]);
        if(hotPoints[0].getY() < hotPoints[1].getY()) {
            return new Rectangle(hotPoints[0].getX(),
            hotPoints[0].getY(),
            (int)Math.abs(diff.getX()), 
            (int)Math.abs(diff.getY()));
        }else {
            return new Rectangle(hotPoints[0].getX(),
            hotPoints[1].getY(),
            (int)Math.abs(diff.getX()), 
            (int)Math.abs(diff.getY()));
        }
    }

    //Mozda treba kopirati i selected!
    @Override
    public GraphicalObject duplicate() {
        return new LineSegment(hotPoints[0], hotPoints[1]);
    }

    @Override
    public String getShapeName() {
        return "Linija";
    }

    @Override
    public String getShapeID() {
        return "@LINE";
    }

    @Override
    public void render(Renderer r) {
        r.drawLine(hotPoints[0], hotPoints[1]);
    }

    @Override
    public void load(Stack<GraphicalObject> stack, String data) {
        String[] params = data.split(" ");

        int x1 = Integer.parseInt(params[0]);
        int y1 = Integer.parseInt(params[1]);
        int x2 = Integer.parseInt(params[2]);
        int y2 = Integer.parseInt(params[3]);

        stack.push(new LineSegment(new Point(x1, y1), new Point(x2, y2)));
    }

    @Override
    public void save(List<String> rows) {
        rows.add(getShapeID() + " " + hotPoints[0].getX() + " " +
        hotPoints[0].getY() + " " + hotPoints[1].getX() + " " + 
        hotPoints[1].getY());
    }

}
