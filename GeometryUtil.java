package vector_draw_program;

import vector_draw_program.helpClasses.Point;

public class GeometryUtil {

	public static double distanceFromPoint(Point point1, Point point2) {
        Point difference = point1.difference(point2);

        return Math.sqrt(Math.pow(difference.getX(), 2) + Math.pow(difference.getY(), 2));
	}
	

    public static double distanceFromLineSegment(Point s, Point e, Point p) {

        if(s.getX() == e.getX()) {
            if((p.getY() > s.getY() && p.getY() > e.getY()) || p.getY() < s.getY() && p.getY() < e.getY()) {
                return Math.min(distanceFromPoint(s, p), distanceFromPoint(e, p));
            }
        }

        if(s.getY() == e.getY()) {
            if((p.getX() > s.getX() && p.getX() > e.getX()) || p.getX() < s.getX() && p.getX() < e.getX()) {
                return Math.min(distanceFromPoint(s, p), distanceFromPoint(e, p));
            }
        }
        double A = p.getX() - s.getX(); // position of point rel one end of line
        double B = p.getY() - s.getY();
        double C = e.getX() - s.getX(); // vector along line
        double D = e.getY() - s.getY();
        double E = -D; // orthogonal vector
        double F = C;
    
        double dot = A * E + B * F;
        double len_sq = E * E + F * F;
    
        return (double) Math.abs(dot) / Math.sqrt(len_sq);

      }
}